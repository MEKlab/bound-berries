# Bound Berries

Collection of scripts to plot data from Timelapse experiments, including:
- Spot-detection for RecB-Halo with long exposure time
- SOS signal quantification
- Nucleoid size quantification

Most data processing (spot detection, nucleoid segmentation, signal quantification...) is to be done in Bacmman and exported as "Measurements".

Associated Bacmman configurations can be found in Bacmman under "Import/Export", "Online Configuration Library", using "el-karoui-lab" as username (no password).
