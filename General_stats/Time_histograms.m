% Script to plot 3D-histograms of cell length, SOS signal and fraction of
% cells with spots over time
%
% Required measurements:
% *ObjectInclusionCount* -> Containing: Bacteria; To count: Spot_detection -> Name: SpotCount
% *ObjectFeatures* -> Object class: Bacteria -> Feature: mean -> Intensity: SOS -> Name: MeanSOS
% *SpineFeatures* -> Bacteria; Scaled: um
%
%

cd(fileparts(matlab.desktop.editor.getActiveFilename)) % cd to script's current folder
addpath('../../Bacmman_scripts/Functions/')

close all
clc

%% Input

dataset = '../../../Matlab/BB_inputs/220322_cipro_30ngmL';


%% Options

% Metadata (will be added to measurements table and can be used for filtering)
varToAdd = {'DateTime'}; % Metadata variables to add (e.g. 'DateTime')
channels = [0]; % Index of table containing varToAdd

% Filters (look only at a subset of the data)
% Multiple filters can be defined using arrays; leave arrays empty to apply no filter
filters.var_to_filter = {}; % Measurement names (e.g. 'SpineLength')
filters.source_table = []; % Index of the table that contains "var_to_filter" in your call to apply_filters
filters.comparison_method = {}; % '>', '<', '=' (e.g. '>' to keep var > threshold)
filters.filter_threshold = []; % Threshold value for the filters

% Time interval for each curve
time_interval = 20; % (min)

% Colormap
colors = [0 0 1     % Start
          0.5 0 0.5   % Middle
          1 0 0] ;  % End


%% Import data
load(dataset)

bact = import_measurements(files.bacmman_folder, 0);

% Add metadata
bact = add_metadata_info(files.bacmman_folder, varToAdd, channels, bact);

% Apply user-defined filters
bact = apply_filters(filters, bact);

% Convert time values
bact.DateTime_c0 = datetime(bact.DateTime_c0, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS');
bact.Time = bact.DateTime_c0 - bact.DateTime_c0(1);
timepoints = minutes(unique(bact.Time));


%% Compute metrics

% General
nCells = grpstats(bact.Idx, bact.PositionIdx, 'max') +1; % Number of cells per FOV

edges = 0:time_interval:max(timepoints)+time_interval;
discrete_time = discretize(minutes(bact.Time), edges);


cellLen = [];
binRange_cellLen = 0:0.2:30;
sos = [];
binRange_sos = 0:10:3000;
spotCount = [];
binRange_spotCount = 0:5;


for i = 1:length(unique(discrete_time))

    % Cell length
    cellLen(:,i) = histcounts(bact.SpineLength(discrete_time == i), [binRange_cellLen Inf], 'Normalization', 'probability');

    % SOS signal
    sos(:,i) = histcounts(bact.MeanSOS(discrete_time == i), [binRange_sos Inf], 'Normalization', 'probability');

    % Fraction of cells with spots
    spotCount(:,i) = histcounts(bact.SpotCount(discrete_time == i), [binRange_spotCount Inf], 'Normalization', 'probability');

end

%Define colormap
n = length(unique(discrete_time)); % size of new color map
t0 = linspace(0,1,3)';
t = linspace(0,1,n)';
r = interp1(t0,colors(:,1),t);
g = interp1(t0,colors(:,2),t);
b = interp1(t0,colors(:,3),t);
cmap = [r,g,b];


%% Make figures

[f1, f2, f3] = Init_figures;

figure(f1)
for i = 1:length(unique(discrete_time))
    plot(binRange_cellLen, smooth(cellLen(:,i), 0.1), 'LineWidth', 1, 'Color', cmap(i,:));
end
xlabel('Cell length')
ylabel('PDF')
hold off
leg = legend(split(num2str(edges(unique(discrete_time)))), 'Interpreter', 'none');
title(leg, 'Time (min)')
legend('boxoff')


figure(f2)
for i = 1:length(unique(discrete_time))
    plot(binRange_sos(1:end-1), smooth(sos(1:end-1,i), 0.1), 'LineWidth', 1, 'Color', cmap(i,:));
end
xlabel('SOS signal (AU)')
ylabel('PDF')
hold off
leg = legend(split(num2str(edges(unique(discrete_time)))), 'Interpreter', 'none');
title(leg, 'Time (min)')
legend('boxoff')


figure(f3)
b = bar(binRange_spotCount, spotCount);
for i = 1:length(unique(discrete_time))
    set(b(i),'FaceColor', cmap(i,:));
end
xlabel('Number of spots per cell')
ylabel('PDF')
hold off
leg = legend(split(num2str(edges(unique(discrete_time)))), 'Interpreter', 'none');
title(leg, 'Time (min)')
legend('boxoff')
