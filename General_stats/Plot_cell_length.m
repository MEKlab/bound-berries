% Make histograms of SOS signal and SOS signal vs. time
%
% Required measurements:
% *SpineFeatures* -> Bacteria; Scaled: um
%

cd(fileparts(matlab.desktop.editor.getActiveFilename)) % cd to script's current folder
addpath('../../Bacmman_scripts/Functions/')
addpath('../Util/')

close all
clc

%% Input

datasets = {'220607_cipro_0', '220607_cipro_30ngmL'};

%% Options

% Metadata (will be added to measurements table and can be used for filtering)
varToAdd = {'DateTime'}; % Metadata variables to add (e.g. 'DateTime')
channels = [0]; % Index of table containing var_to_add

% Filters (look only at a subset of the data)
% Multiple filters can be defined using arrays; leave arrays empty to apply no filter
filters.var_to_filter = {}; % Measurement names
filters.source_table = []; % Index of the table that contains "var_to_filter"
filters.comparison_method = {}; % '>', '<', '=' (e.g. '>' to keep var > threshold)
filters.filter_threshold = [];

% Moving mean span
movmean_span = 20;

% Bins for histograms (start:step:end)
binRange1 = 0:0.2:30; % Cell length
binRange2 = 0:0.05:30; % Cell width

% Define a colormap for the figures (must have at least as many colors as datasets)
cmap = [[255 193 7];...
        [30 136 229];...
        [0 77 64];...
        [216 27 96]]./255;


%% Processing

% Initialise figures
[f1, f2, f3] = Init_figures;
p1 = [];
p2 = [];
p3 = [];

av_spineLength = {};
av_spineWidth = {};

for a = 1:length(datasets)

    disp(['Loading dataset ' datasets{a} '...'])
    
    % Load input for current dataset
    load(['../../../Matlab/BB_inputs/' datasets{a}])


    %% Import and filter data
    bact = import_measurements(files.bacmman_folder, 0);

    % Add metadata
    bact = add_metadata_info(files.bacmman_folder, varToAdd, channels, bact);

    % Apply user-defined filters
    bact = apply_filters(filters, bact);

    %% Data processing

    % Convert time values
    bact.DateTime_c0 = datetime(bact.DateTime_c0, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS');
    timepoints = unique(bact.DateTime_c0 - bact.DateTime_c0(1));

    % Number of cells per image
    nCells = grpstats(bact.Idx, bact.PositionIdx, 'max') +1;

    % Cell length vs time
    cellLen = grpstats(bact.SpineLength, bact.PositionIdx, 'mean');
    length_movmean = Weighted_movmean(cellLen, movmean_span, nCells);

    av_spineLength{a} = num2str(mean(rmmissing(bact.SpineLength)),'%.1f');
    av_spineWidth{a} = num2str(mean(rmmissing(bact.SpineWidth)),'%.1f');

    %% General statistics
    disp(['Total number of cells: ' num2str(height(bact)) ' (' num2str(mean(nCells), '%.0f') ' per image on average)'])
    disp(['Average cell length: ' num2str(av_spineLength{a},'%.1f') ' um'])
    disp(['Average cell width: ' num2str(av_spineWidth{a},'%.1f') ' um'])

    %% Make figures

    % Histogram of cell lengths
    p1(a,:) = histcounts(bact.SpineLength,[binRange1 Inf], 'Normalization', 'probability');

    % Histogram of cell widths
    p2(a,:) = histcounts(bact.SpineWidth,[binRange2 Inf], 'Normalization', 'probability');

    % Cell length vs time
    figure(f3)
    scatter(minutes(timepoints), cellLen, 15, 'filled', 'MarkerFaceAlpha', 0.8, 'MarkerFaceColor', cmap(a,:));
    p3(a) = plot(minutes(timepoints), length_movmean, 'LineWidth', 1.5, 'Color', cmap(a,:));
    box on
    xlabel('Time (min)')
    ylabel('Average cell length (um)')
    drawnow

    fprintf('\n')

end

% Update figures
figure(f1)
b1 = plot(binRange1(1:end-1), p1(:,1:end-1), 'LineWidth', 1);
for i = 1:length(datasets)
    set(b1(i),'Color', cmap(i,:));
end
xlim([0 15])
xlabel('Cell length (um)')
ylabel('PDF')
hold off
leg1 = legend(b1, av_spineLength, 'Interpreter', 'none');
title(leg1, 'Average cell length (um)')
legend('boxoff')

figure(f2)
b2 = plot(binRange2(1:end-1), p2(:,1:end-1), 'LineWidth', 1);
for i = 1:length(datasets)
    set(b2(i),'Color', cmap(i,:));
end
xlim([0 5])
xlabel('Cell length (um)')
ylabel('PDF')
hold off
leg1 = legend(b2, av_spineWidth, 'Interpreter', 'none');
title(leg1, 'Average cell width (um)')
legend('boxoff')

figure(f3)
legend(p3, datasets, 'Interpreter', 'none')
legend('boxoff')
