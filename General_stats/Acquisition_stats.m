% Plot different metrics linked to acquisition
%

cd(fileparts(matlab.desktop.editor.getActiveFilename)) % cd to script's current folder
addpath('../../Bacmman_scripts/Functions/')

close all
clc

%% Input
dataset = '../../../Matlab/BB_inputs/220310_cipro_300ngmL';

%% Options

% Metadata (will be added to measurements table and can be used for filtering)
varToAdd = {'ASI X', 'ASI Y', 'DateTime'}; % Metadata variables to add (e.g. 'DateTime')
channels = [0, 0, 0]; % Index of table containing varToAdd

% Filters (look only at a subset of the data)
% Multiple filters can be defined using arrays; leave arrays empty to apply no filter
filters.var_to_filter = {}; % Measurement names (e.g. 'SpineLength')
filters.source_table = []; % Index of the table that contains "var_to_filter" in your call to apply_filters
filters.comparison_method = {}; % '>', '<', '=' (e.g. '>' to keep var > threshold)
filters.filter_threshold = []; % Threshold value for the filters


%% Import and filter data
load(dataset)

bact = import_measurements(files.bacmman_folder, 0);

% Add metadata
bact = add_metadata_info(files.bacmman_folder, varToAdd, channels, bact);

% Apply user-defined filters
bact = apply_filters(filters, bact);

% Convert time values
bact.DateTime_c0 = datetime(bact.DateTime_c0, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS');
bact.Time = bact.DateTime_c0 - bact.DateTime_c0(1);

% Convert ASI positions to numbers
bact.ASIX_c0 = str2double(bact.ASIX_c0);
bact.ASIY_c0 = str2double(bact.ASIY_c0);


%% General statistics

% Number of cells per image
nCells = grpstats(bact.Idx, bact.PositionIdx, 'max') +1;

disp(['Number of images: ' num2str(length(unique(bact.Position)))])
disp(['Total acquisition time: ' num2str(hours(max(bact.Time)), '%.1f') ' hours'])
disp(['Total number of cells: ' num2str(height(bact)) ' (' num2str(mean(nCells), '%.0f') ' per image on average)'])


%% Make figures

% Number of cells recorded per image
figure('color','white', 'Position', [560 558 560 420])
scatter(unique(bact.PositionIdx), nCells, 15, 'filled')
box on
xlabel('Image number')
ylabel('Number of cells recorded')
ylim([0 1.2*max(nCells)])


% Scheme of the acquisition pattern
figure('Color', 'white', 'Position', [1120 558 560 420])
hold on
plot(bact.ASIX_c0, bact.ASIY_c0, 'Color', 'k')
scatter(bact.ASIX_c0, bact.ASIY_c0, 10, 'MarkerFaceColor', 'b', 'MarkerEdgeColor', 'b')
scatter(bact.ASIX_c0(1), bact.ASIY_c0(1), 30, 'r')
hold off
box on
set(gca, 'YDir','reverse')
set(gca, 'XDir','reverse')
xlabel('X position (um)')
ylabel('Y position (um)')
legend('Acquisition path', 'Images acquired', 'First image')














