% Script to plot the evolution of RecA states (diffuse, spot or filament) over time
%
% States are to be obtained from the Python deep-learning model


cd(fileparts(matlab.desktop.editor.getActiveFilename)) % cd to script's current folder
addpath('../../Bacmman_scripts/Functions/')
addpath('../Util/')

close all
clc

%% Input

datasets = {'220407_cipro_0','220629_DT19_cipro_30ngmL','220407_cipro_300ngmL'};

%% Options

% Metadata (will be added to measurements table and can be used for filtering)
varToAdd = {'DateTime'}; % Metadata variables to add (e.g. 'DateTime')
channels = [0]; % Index of table containing varToAdd

% Filters (look only at a subset of the data)
% Multiple filters can be defined using arrays; leave arrays empty to apply no filter
filters.var_to_filter = {'TimeMin'}; % Measurement names (e.g. 'SpineLength')
filters.source_table = [0]; % Index of the table that contains "var_to_filter" in your call to apply_filters
filters.comparison_method = {'<'}; % '>', '<', '=' (e.g. '>' to keep var > threshold)
filters.filter_threshold = [21]; % Threshold value for the filters

% Moving mean span
movmean_span = 10;

% Define a colormap for the figures (must have at least as many colors as datasets)
cmap = [[255 193 7];...
        [30 136 229];...
        [216 27 96];...
        [0 77 64]]./255;

%% Processing

% Initialise figures
[f1, f2, f3] = Init_figures;
p1 = [];
p2 = [];
p3 = [];

for a = 1:length(datasets)

    disp(['Loading dataset ' datasets{a} '...'])

    % Load input for current dataset
    load(['../../../Matlab/BB_inputs/' datasets{a}])


    %% Import data
    bact = import_measurements(files.bacmman_folder, 0);

    % Add metadata
    bact = add_metadata_info(files.bacmman_folder, varToAdd, channels, bact);

    % Convert time values
    bact.DateTime_c0 = datetime(bact.DateTime_c0, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS');
    bact.Time = bact.DateTime_c0 - bact.DateTime_c0(1);
    bact.TimeMin = minutes(bact.Time);

    % Apply user-defined filters
    bact = apply_filters(filters, bact);

    timepoints = unique(bact.Time);

    %% Compute metrics
    nCells = grpstats(bact.Idx, bact.PositionIdx, 'max') +1; % Number of cells per FOV

    diffuseFrac = grpstats(bact.Category == 1, bact.PositionIdx, 'sum')./nCells; % Proportion of cells with spots per FOV
    diffuseFrac_mean = Weighted_movmean(diffuseFrac, movmean_span, nCells);

    filamentFrac = grpstats(bact.Category == 2, bact.PositionIdx, 'sum')./nCells; % Proportion of cells with spots per FOV
    filamentFrac_mean = Weighted_movmean(filamentFrac, movmean_span, nCells);

    spotFrac = grpstats(bact.Category == 3, bact.PositionIdx, 'sum')./nCells; % Proportion of cells with spots per FOV
    spotFrac_mean = Weighted_movmean(spotFrac, movmean_span, nCells);

    %% General statistics
    disp(['Total number of cells: ' num2str(sum(nCells)) ' (' num2str(round(mean(nCells))) ' per FOV on average)'])
    disp(['Average proportion of cells with a filament per FOV: ' num2str(mean(filamentFrac)*100,2) '%'])

    %% Make figures

    % Time evolution of the fraction of cells with diffuse RecA per FOV
    figure(f1)
    hold on
    scatter(minutes(timepoints), diffuseFrac, 15, 'filled', 'MarkerFaceAlpha', 0.9, 'MarkerFaceColor', cmap(a,:));
    p1(a) = plot(minutes(timepoints), diffuseFrac_mean, 'LineWidth', 1.5, 'Color', cmap(a,:));
    hold off
    box on
    xlabel('Time (min)')
    ylabel('Proportion of cells containing diffuse RecA')
    ylim([0 1])
    drawnow

    % Time evolution of the fraction of cells with a RecA filament per FOV
    figure(f2)
    hold on
    scatter(minutes(timepoints), filamentFrac, 15, 'filled', 'MarkerFaceAlpha', 0.9, 'MarkerFaceColor', cmap(a,:));
    p2(a) = plot(minutes(timepoints), filamentFrac_mean, 'LineWidth', 1.5, 'Color', cmap(a,:));
    hold off
    box on
    xlabel('Time (min)')
    ylabel('Proportion of cells containing a RecA filament')
    ylim([0 1])
    drawnow

    % Time evolution of the fraction of cells with a RecA spot per FOV
    figure(f3)
    hold on
    scatter(minutes(timepoints), spotFrac, 15, 'filled', 'MarkerFaceAlpha', 0.9, 'MarkerFaceColor', cmap(a,:));
    p3(a) = plot(minutes(timepoints), spotFrac_mean, 'LineWidth', 1.5, 'Color', cmap(a,:));
    hold off
    box on
    xlabel('Time (min)')
    ylabel('Proportion of cells containing a RecA spot')
    ylim([0 1])
    drawnow

    fprintf('\n')

end

% Update figures
figure(f1)
hold off
title('Diffuse RecA')
legend(p1, datasets, 'Interpreter', 'none')
legend('boxoff')

figure(f2)
hold off
title('RecA filament')
legend(p2, datasets, 'Interpreter', 'none')
legend('boxoff')

figure(f3)
hold off
title('RecA spot')
legend(p3, datasets, 'Interpreter', 'none')
legend('boxoff')
