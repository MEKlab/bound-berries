% Make histograms of RecA-SYFP2 intensity and RecA-SYFP2 intensity vs. time
%
% Required measurements:
% *ObjectFeatures* -> Object class: Bacteria -> Feature: mean -> Intensity: RecA -> Name: MeanRecAIntensity
%

cd(fileparts(matlab.desktop.editor.getActiveFilename)) % cd to script's current folder
addpath('../../Bacmman_scripts/Functions/')

close all
clc

%% Input

datasets = {'220407_cipro_300ngmL_clean'};

%% Options

% Metadata (will be added to measurements table and can be used for filtering)
varToAdd = {'DateTime'}; % Metadata variables to add (e.g. 'DateTime')
channels = [0]; % Index of table containing var_to_add

% Filters (look only at a subset of the data)
% Multiple filters can be defined using arrays; leave arrays empty to apply no filter
filters.var_to_filter = {'RecA_cat', 'Time'}; % Measurement names (e.g. 'SpineLength')
filters.source_table = [0, 0]; % Index of the table that contains "var_to_filter"
filters.comparison_method = {'=', '>'}; % '>', '<', '=' (e.g. '>' to keep var > threshold)
filters.filter_threshold = [0, 30]; % Threshold value for the filters

% Bins for histogram (start:step:end)
binRange = 0:300:25000;

% Define a colormap for the figures (must have at least as many colors as datasets)
cmap = [[255 193 7];...
        [30 136 229];...
        [0 77 64];...
        [216 27 96]]./255;


%% Processing

% Initialise figures
[f1, f2] = Init_figures;
p1 = [];
p2 = [];

for a = 1:length(datasets)

    disp(['Loading dataset ' datasets{a} '...'])

    % Load input for current dataset
    load(['../../../Matlab/BB_inputs/' datasets{a}])


    %% Import and filter data
    bact = import_measurements(files.bacmman_folder, 0);

    % Add metadata
    bact = add_metadata_info(files.bacmman_folder, varToAdd, channels, bact);
    % Convert time values
    bact.DateTime_c0 = datetime(bact.DateTime_c0, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS');
    bact.Time = minutes(bact.DateTime_c0 - bact.DateTime_c0(1));

    % Apply user-defined filters
    bact = apply_filters(filters, bact);

    %% Data processing

    timepoints = unique(bact.Time);

    % Compute average fluorescence intensity per image
    recA = grpstats(bact.MeanRecAIntensity, bact.PositionIdx, 'mean');
    recA_movmean = movmean(recA, 20);

    %% General statistics
    disp(['Average RecA-SYFP2 intensity of dataset: ' num2str(mean(bact.MeanRecAIntensity),'%.0f')])

    %% Make figures

    % Histogram of RecA-SYFP2 signal
    p1(a,:) = histcounts(bact.MeanRecAIntensity,[binRange Inf], 'Normalization', 'probability');

    % Fluorescence intensity vs time
    figure(f2)
    scatter(minutes(timepoints), recA, 15, 'filled', 'MarkerFaceAlpha', 0.8, 'MarkerFaceColor', cmap(a,:));
    p2(a) = plot(minutes(timepoints), recA_movmean, 'LineWidth', 1.5, 'Color', cmap(a,:));
    box on
    xlabel('Time (min)')
    ylabel('Average RecA-SYFP2 intensity')
    drawnow

    fprintf('\n')

end

% Update figures
figure(f1)
b = bar(binRange(1:end-1), p1(:,1:end-1), 0.7*length(datasets));
for i = 1:length(datasets)
    set(b(i),'FaceColor', cmap(i,:));
end
xlabel('Mean RecA-SYFP2 intensity per cell')
ylabel('PDF')
hold off
legend(datasets, 'Interpreter', 'none')
legend('boxoff')


figure(f2)
hold off
legend(p2, datasets, 'Interpreter', 'none')
legend('boxoff')
