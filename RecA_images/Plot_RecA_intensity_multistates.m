% Make histograms of RecA-SYFP2 intensity and RecA-SYFP2 intensity vs. time
%
% Required measurements:
% *ObjectFeatures* -> Object class: Bacteria -> Feature: mean -> Intensity: RecA -> Name: MeanRecAIntensity
%

cd(fileparts(matlab.desktop.editor.getActiveFilename)) % cd to script's current folder
addpath('../../Bacmman_scripts/Functions/')

close all
clc

%% Input

dataset = '220407_cipro_300ngmL_clean';

%% Options

% Metadata (will be added to measurements table and can be used for filtering)
varToAdd = {'DateTime'}; % Metadata variables to add (e.g. 'DateTime')
channels = [0]; % Index of table containing var_to_add

% Filters (look only at a subset of the data)
% Multiple filters can be defined using arrays; leave arrays empty to apply no filter
filters.var_to_filter = {'Time'}; % Measurement names (e.g. 'SpineLength')
filters.source_table = [0]; % Index of the table that contains "var_to_filter"
filters.comparison_method = {'>'}; % '>', '<', '=' (e.g. '>' to keep var > threshold)
filters.filter_threshold = [30]; % Threshold value for the filters

% Bins for histogram (start:step:end)
binRange = 0:300:25000;

% Define a colormap for the figures (must have at least as many colors as datasets)
cmap = [[255 193 7];...
        [17 119 51];...
        [254 97 0];...
        [30 136 229]]./255;


%% Processing

% Initialise figures
[f1, f2, f3, f4] = Init_figures;
p1 = [];
p2 = [];
p3 = [];
p4 = [];


% Load input for current dataset
load(['../../../Matlab/BB_inputs/' dataset])


%% Import and filter data
bact = import_measurements(files.bacmman_folder, 0);

% Add metadata
bact = add_metadata_info(files.bacmman_folder, varToAdd, channels, bact);
% Convert time values
bact.DateTime_c0 = datetime(bact.DateTime_c0, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS');
bact.Time = minutes(bact.DateTime_c0 - bact.DateTime_c0(1));

% Apply user-defined filters
bact = apply_filters(filters, bact);

%% Data processing

timepoints = unique(bact.Time);
timepoints_diff = unique(bact.Time(bact.RecA_cat == 0));
timepoints_fil = unique(bact.Time(bact.RecA_cat == 1));
timepoints_spot = unique(bact.Time(bact.RecA_cat == 2));

% Compute average fluorescence intensity per image
recA = grpstats(bact.MeanRecAIntensity, bact.PositionIdx, 'mean');
recA_diff = grpstats(bact.MeanRecAIntensity(bact.RecA_cat == 0), bact.PositionIdx(bact.RecA_cat == 0), 'mean');
recA_fil = grpstats(bact.MeanRecAIntensity(bact.RecA_cat == 1), bact.PositionIdx(bact.RecA_cat == 1), 'mean');
recA_spot = grpstats(bact.MeanRecAIntensity(bact.RecA_cat == 2), bact.PositionIdx(bact.RecA_cat == 2), 'mean');

recA_movmean = movmean(recA, 20);
recA_diff_movmean = movmean(recA_diff, 20);
recA_fil_movmean = movmean(recA_fil, 20);
recA_spot_movmean = movmean(recA_spot, 20);

%% General statistics
disp(['Average RecA-SYFP2 intensity of dataset: ' num2str(mean(bact.MeanRecAIntensity),'%.0f')])

disp(['Average RecA-SYFP2 intensity (diffuse RecA): ' num2str(mean(bact.MeanRecAIntensity(bact.RecA_cat == 0)),'%.0f')])
disp(['Average RecA-SYFP2 intensity (RecA filament): ' num2str(mean(bact.MeanRecAIntensity(bact.RecA_cat == 1)),'%.0f')])
disp(['Average RecA-SYFP2 intensity (RecA spot): ' num2str(mean(bact.MeanRecAIntensity(bact.RecA_cat == 2)),'%.0f')])

%% Make figures

% Histogram of RecA-SYFP2 signal (overall)
figure(f1)
p1(1,:) = histcounts(bact.MeanRecAIntensity,[binRange Inf], 'Normalization', 'probability');
b = bar(binRange(1:end-1), p1(:,1:end-1));
set(b,'FaceColor', cmap(1,:));
xlabel('RecA-SYFP2 intensity/um2')
ylabel('PDF')
hold off
legend(dataset, 'Interpreter', 'none')
legend('boxoff')

% Histogram of RecA-SYFP2 signal (split populations)
figure(f2)
hold on
for i = 1:3
    p2(i,:) = histcounts(bact.MeanRecAIntensity(bact.RecA_cat == i-1),[binRange Inf], 'Normalization', 'probability');
end
b = bar(binRange(1:end-1), p2(:,1:end-1)', 0.4);
p = plot(binRange(1:end-1), p2(:,1:end-1)');
for i = 1:3
    set(b(i),'FaceColor', cmap(i+1,:));
    set(p(i),'Color', cmap(i+1,:));
end
xlabel('RecA-SYFP2 intensity/um2')
ylabel('PDF')
hold off
legend({'Diffuse RecA','RecA filament','RecA spot'}, 'Interpreter', 'none')
legend('boxoff')


% Fluorescence intensity vs time (overall)
figure(f3)
scatter(minutes(timepoints), recA, 15, 'filled', 'MarkerFaceAlpha', 0.8, 'MarkerFaceColor', cmap(1,:));
p3(1) = plot(minutes(timepoints), recA_movmean, 'LineWidth', 1.5, 'Color', cmap(1,:));
box on
xlabel('Time (min)')
ylabel('RecA-SYFP2 intensity/um2')
hold off
legend(p3, dataset, 'Interpreter', 'none')
legend('boxoff')


% Fluorescence intensity vs time (split populations)
figure(f4)
scatter(minutes(timepoints_diff), recA_diff, 15, 'filled', 'MarkerFaceAlpha', 0.8, 'MarkerFaceColor', cmap(2,:));
p4(1) = plot(minutes(timepoints_diff), recA_diff_movmean, 'LineWidth', 1.5, 'Color', cmap(2,:));

scatter(minutes(timepoints_fil), recA_fil, 15, 'filled', 'MarkerFaceAlpha', 0.8, 'MarkerFaceColor', cmap(3,:));
p4(2) = plot(minutes(timepoints_fil), recA_fil_movmean, 'LineWidth', 1.5, 'Color', cmap(3,:));

scatter(minutes(timepoints_spot), recA_spot, 15, 'filled', 'MarkerFaceAlpha', 0.8, 'MarkerFaceColor', cmap(4,:));
p4(3) = plot(minutes(timepoints_spot), recA_spot_movmean, 'LineWidth', 1.5, 'Color', cmap(4,:));

box on
xlabel('Time (min)')
ylabel('RecA-SYFP2 intensity/um2')
hold off
legend(p4, {'Diffuse RecA','RecA filament','RecA spot'}, 'Interpreter', 'none')
legend('boxoff')




