%%% Use this script to save input paths to file and save them as a .mat
%%% file for easy import in other scripts of bound-berries

clear

%% Input

input_name = '220310_cipro_300ngmL';

% Bacmman folder (contains segmentation output)
files.bacmman_folder = '../../../BACMMAN/Timelapse/220310_cipro300ngmL/';
files.file_bact = '220310_cipro300ngmL_0'; % Table with bacteria measurements from Bacmman
files.file_spots = '220310_cipro300ngmL_1'; % Spots measurements table
files.file_nucl = '220310_cipro300ngmL_2'; % Nucleoid measurements table

files.prefix = 'Im'; % Prefix to the image files (e.g. 'Im' for Im001, Im002,...)

% Segmentation output (hdf5 file) for cells, nucleoids and spots
segFile_Name = 'Segmentation.h5';
cell_featureName = 'Cells';
nucleoid_featureName = 'Nucleoid';
spots_featureName = 'Spots';

%% Checks

disp(['Generating input "' input_name '"'])

% Check that path points to existing Bacmman tables
err = 0;
if ~exist([files.bacmman_folder files.file_bact '.csv'], 'file')
    warning('Cells measurements not found')
    err=1;
end
if ~exist([files.bacmman_folder files.file_spots '.csv'], 'file')
    warning('Spots measurements not found')
    err=1;
end
    
if err == 0
    disp('Bacmman files: OK')
end

%% Save variables

save(['../../../Matlab/BB_inputs/' input_name])