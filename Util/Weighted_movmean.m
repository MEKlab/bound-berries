% Function to return a weighted moving mean

function mov_mean = Weighted_movmean(data, window, weights)
    mov_mean = zeros(length(data), 1);
    for i = 1:length(data)
        edge_low = max(i - round(window/2), 1);
        edge_high = min(i + round(window/2), length(data));
        mov_mean(i) = sum(repelem(data(edge_low:edge_high), weights(edge_low:edge_high))) / sum(weights(edge_low:edge_high));
    end
end