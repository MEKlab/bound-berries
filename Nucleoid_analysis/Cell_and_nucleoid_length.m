% Make a plot that combines cell length, nucleoid length (assuming it's a sausagoid like the cell) and spot position


cd(fileparts(matlab.desktop.editor.getActiveFilename)) % cd to script's current folder
addpath('../../Bacmman_scripts/Functions/')

close all
clc

%% Input

dataset = '../../../Matlab/BB_inputs/220310_cipro_0';


%% Options

% Metadata (will be added to measurements table and can be used for filtering)
varToAdd = {'DateTime'}; % Metadata variables to add (e.g. 'DateTime')
channels = [0]; % Index of table containing varToAdd

% Filters (look only at a subset of the data)
% Multiple filters can be defined using arrays; leave arrays empty to apply no filter
filters.var_to_filter = {}; % Measurement names (e.g. 'SpineLength')
filters.source_table = []; % Index of the table that contains "var_to_filter" in your call to apply_filters
filters.comparison_method = {}; % '>', '<', '=' (e.g. '>' to keep var > threshold)
filters.filter_threshold = []; % Threshold value for the filters

% Plot options
plot_title = 'No cipro';

%% Import data
load(dataset)

[bact, spots, nucl] = import_measurements(files.bacmman_folder, [0, 1, 2]);

% Add metadata
[bact, spots, nucl] = add_metadata_info(files.bacmman_folder, varToAdd, channels, bact, spots, nucl);

% Apply user-defined filters
[bact, spots, nucl] = apply_filters(filters, bact, spots, nucl);

% Convert time values
bact.DateTime_c0 = datetime(bact.DateTime_c0, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS');
bact.Time = bact.DateTime_c0 - bact.DateTime_c0(1);
nucl.DateTime_c0 = datetime(nucl.DateTime_c0, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS');
nucl.Time = nucl.DateTime_c0 - nucl.DateTime_c0(1);
spots.DateTime_c0 = datetime(spots.DateTime_c0, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS');
spots.Time = spots.DateTime_c0 - spots.DateTime_c0(1);

% Remove NaNs from nucleoid table (update bact and spots in consequence)
nucl = rmmissing(nucl);
bact = bact(ismember([bact.PositionIdx, bact.Indices_2], [nucl.PositionIdx, nucl.Indices_2], 'row'),:);
spots = spots(ismember([spots.PositionIdx, spots.Indices_2], [nucl.PositionIdx, nucl.Indices_2], 'row'),:);

% Centre spot coordinates
spots.SpineCurvilinearCoord = spots.SpineCurvilinearCoord -0.5*spots.SpineLength;

timepoints = minutes(unique(bact.Time));
timepoints_spots = minutes(unique(spots.Time));

% If a cell has several nucleoids, sum up their lengths
% This is not ideal (e.g. for fragmented nucleoids) but in my case I believe most "multiple nucleoids" are actually touching
nuclByCell = grpstats(nucl(:,2:end),...
                      {'PositionIdx', 'Indices_2'}, 'sum',...
                       'DataVars', {'SpineLength'},...
                       'VarNames', {'PositionIdx', 'Indices_2', 'GroupCount', 'Nucleoid_length'});
bact = join(bact, nuclByCell(:,{'PositionIdx','Indices_2','Nucleoid_length'}));



%% Compute metrics

av_bactLength = grpstats(bact.SpineLength, bact.PositionIdx, 'mean');
av_nuclLength = grpstats(bact.Nucleoid_length, bact.PositionIdx, 'mean');
av_spotPos = grpstats(spots.SpineCurvilinearCoord, spots.PositionIdx, 'mean');


%% Make figures

f1 = Init_figures;

figure(f1)
hold on
p(1) = plot(timepoints, smooth(av_bactLength, 20)/2, 'k');
plot(timepoints, smooth(-av_bactLength, 20)/2, 'k')

p(2) = area(timepoints, smooth(av_nuclLength, 20)/2, 'FaceColor', 'b', 'FaceAlpha', 0.3, 'EdgeAlpha', 0, 'ShowBaseLine', 'off');
area(timepoints, smooth(-av_nuclLength, 20)/2, 'FaceColor', 'b', 'FaceAlpha', 0.3, 'EdgeAlpha', 0, 'ShowBaseLine', 'off');

p(3) = plot(timepoints_spots, av_spotPos, '.r');

hold off
xlabel('Time (min)')
ylabel('Length (um)')
legend(p, 'Cell edges', 'Nucleoid', 'Average spot position')

title(plot_title)
