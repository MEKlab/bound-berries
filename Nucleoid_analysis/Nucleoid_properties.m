% Make plots of nucleoid size over time
%
% Required measurements:
% *ObjectFeatures* -> Object class: Nucleoid -> Feature: Size -> Name: NucleoidSize
% *ObjectFeatures* -> Object class: Bacteria -> Feature: Size -> Name: BacteriaSize
% *ObjectInclusionCount* -> Containing: Nucleoid; To count: Spot_detection -> Name: Spots_in_nucleoid
%

cd(fileparts(matlab.desktop.editor.getActiveFilename)) % cd to script's current folder
addpath('../../Bacmman_scripts/Functions/')

close all
clc

%% Input

datasets = {'220310_cipro_0', '220310_cipro_300ngmL'};

%% Options

% Metadata (will be added to measurements table and can be used for filtering)
varToAdd = {'DateTime'}; % Metadata variables to add (e.g. 'DateTime')
channels = [0]; % Index of table containing varToAdd

% Filters (look only at a subset of the data)
% Multiple filters can be defined using arrays; leave arrays empty to apply no filter
filters.var_to_filter = {}; % Measurement names (e.g. 'SpineLength')
filters.source_table = []; % Index of the table that contains "var_to_filter" in your call to apply_filters
filters.comparison_method = {}; % '>', '<', '=' (e.g. '>' to keep var > threshold)
filters.filter_threshold = []; % Threshold value for the filters

% Define a colormap for the figures (must have at least as many colors as datasets)
cmap = [[255 193 7];...
        [30 136 229];...
        [0 77 64];...
        [216 27 96]]./255;

%% Processing

% Initialise figures
[f1, f2, f3, f4, f5, f6] = Init_figures;
p1 = [];
p2 = [];
p3 = [];
p4 = [];
p5 = [];
p6 = [];


for a = 1:length(datasets)

    disp(['Loading dataset ' datasets{a} '...'])

    % Load input for current dataset
    load(['../../../Matlab/BB_inputs/' datasets{a}])


    %% Import data
    [bact,nucl] = import_measurements(files.bacmman_folder, [0,2]);

    % Add metadata
    [bact,nucl] = add_metadata_info(files.bacmman_folder, varToAdd, channels, bact,nucl);

    % Apply user-defined filters
    [bact,nucl] = apply_filters(filters, bact,nucl);

    % Convert time values
    bact.DateTime_c0 = datetime(bact.DateTime_c0, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS');
    bact.Time = bact.DateTime_c0 - bact.DateTime_c0(1);
    timepoints = unique(bact.Time);

    %% Compute metrics

    nSpots = grpstats(bact.SpotCount, bact.PositionIdx, 'sum'); % Number of spots per FOV

    % Total size of nucleoid in the cell (add up if there are several nucleoids)
    % Indices_2 is cell number
    nuclByCell = grpstats(nucl(:,2:end),...
        {'PositionIdx', 'Indices_2'}, 'sum',...
        'DataVars', {'NucleoidSize'},...
        'VarNames',{'PositionIdx', 'Indices_2', 'GroupCount', 'NucleoidSize'});
    bact = join(bact, nuclByCell(:,{'PositionIdx','Indices_2','NucleoidSize'}));

    % Fraction of the cell occupied by the nucleoid
    bact.nucleoidFracSize = bact.NucleoidSize./bact.BacteriaSize;

    % Average nucleoid size/fraction of cell occupied per image
    NucleoidSize = grpstats(bact.NucleoidSize, bact.PositionIdx, 'mean');
    NucleoidSize_movmean = movmean(NucleoidSize, 50);

    nucleoid_fracSize = grpstats(bact.nucleoidFracSize, bact.PositionIdx, 'mean');
    nucleoid_fracSize_movmean = movmean(nucleoid_fracSize, 50);

    % Fraction of spots contained in the nucleoid per FOV
    spots_inNucleoid = grpstats(nucl.Spots_in_nucleoid, nucl.PositionIdx, 'sum')./nSpots;
    spots_inNucleoid_movmean = movmean(rmmissing(spots_inNucleoid), 50);


    %% General statistics
    disp(['Average nucleoid size of dataset: ' num2str(mean(bact.NucleoidSize),'%.1f') ' um^2'])
    disp(['Average fraction of the cell occupied by the nucleoid: ' num2str(mean(bact.nucleoidFracSize)*100,'%.0f') '%'])
    disp(['Fraction of spots contained in the nucleoid: ' num2str(mean(rmmissing(spots_inNucleoid))*100,'%.0f') '%'])

    %% Make figures

    % Histogram of nucleoid size
    binRange1 = 0:0.1:10;
    p1(a,:) = histcounts(bact.NucleoidSize,[binRange1 Inf], 'Normalization', 'probability');

    % Histogram of fraction of cell occupied by nucleoid
    binRange2 = 0:0.05:1;
    p2(a,:) = histcounts(bact.nucleoidFracSize,[binRange2 Inf], 'Normalization', 'probability');

    % Histogram of fraction of cell occupied by nucleoid
    binRange3 = 0:0.05:1;
    p3(a,:) = histcounts(spots_inNucleoid,[binRange3 Inf], 'Normalization', 'probability');

    % Nucleoid size vs time
    figure(f4)
    scatter(minutes(timepoints), NucleoidSize, 15, 'filled', 'MarkerFaceAlpha', 0.8, 'MarkerFaceColor', cmap(a,:));
    p4(a) = plot(minutes(timepoints), NucleoidSize_movmean, 'LineWidth', 1.5, 'Color', cmap(a,:));
    box on
    xlabel('Time (min)')
    ylabel('Average nucleoid size (um^2)')
    drawnow

    % Fraction of cell occupied by nucleoid vs time
    figure(f5)
    scatter(minutes(timepoints), nucleoid_fracSize, 15, 'filled', 'MarkerFaceAlpha', 0.8, 'MarkerFaceColor', cmap(a,:));
    p5(a) = plot(minutes(timepoints), nucleoid_fracSize_movmean, 'LineWidth', 1.5, 'Color', cmap(a,:));
    box on
    xlabel('Time (min)')
    ylabel('Fraction of cell occupied by nucleoid')
    drawnow

    % Fraction of spots contained in the nucleoid vs time
    if sum(~isnan(spots_inNucleoid))
        figure(f6)
        scatter(minutes(timepoints), spots_inNucleoid, 15, 'filled', 'MarkerFaceAlpha', 0.8, 'MarkerFaceColor', cmap(a,:));
        p6(a) = plot(minutes(timepoints(~isnan(spots_inNucleoid_movmean))), spots_inNucleoid_movmean, 'LineWidth', 1.5, 'Color', cmap(a,:));
        box on
        xlabel('Time (min)')
        ylabel('Fraction of spots contained in the nucleoid')
        drawnow
    end

    fprintf('\n')

end


% Update figures
figure(f1)
b = plot(binRange1(1:end-1), p1(:,1:end-1), 'LineWidth', 1);
for i = 1:length(datasets)
    set(b(i),'Color', cmap(i,:));
end
xlabel('Nucleoid size (um^2)')
ylabel('PDF')
hold off
legend(datasets, 'Interpreter', 'none')
legend('boxoff')

figure(f2)
b = plot(binRange2(1:end-1), p2(:,1:end-1), 'LineWidth', 1);
for i = 1:length(datasets)
    set(b(i),'Color', cmap(i,:));
end
xlabel('Fraction of cell occupied by nucleoid')
ylabel('PDF')
hold off
legend(datasets, 'Interpreter', 'none')
legend('boxoff')

figure(f3)
b = plot(binRange3(1:end-1), p3(:,1:end-1), 'LineWidth', 1);
for i = 1:length(datasets)
    set(b(i),'Color', cmap(i,:));
end
xlabel('Fraction of spots contained in the nucleoid')
ylabel('PDF')
hold off
legend(datasets, 'Interpreter', 'none')
legend('boxoff')


figure(f4)
hold off
legend(p4, datasets, 'Interpreter', 'none')
legend('boxoff')

figure(f5)
hold off
legend(p5, datasets, 'Interpreter', 'none')
legend('boxoff')

if sum(~isnan(spots_inNucleoid))
    figure(f6)
    hold off
    legend(p6, datasets, 'Interpreter', 'none')
    legend('boxoff')
end
