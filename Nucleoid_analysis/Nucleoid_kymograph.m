% Make a kymograph of nucleoid length
% 1 line = 1 field-of-view
%
%

cd(fileparts(matlab.desktop.editor.getActiveFilename)) % cd to script's current folder
addpath('../../Bacmman_scripts/Functions/')

close all
clc

%% Input

dataset = '../../../Matlab/BB_inputs/220310_cipro_0';


%% Options

% Metadata (will be added to measurements table and can be used for filtering)
varToAdd = {'DateTime'}; % Metadata variables to add (e.g. 'DateTime')
channels = [0]; % Index of table containing varToAdd

% Filters (look only at a subset of the data)
% Multiple filters can be defined using arrays; leave arrays empty to apply no filter
filters.var_to_filter = {}; % Measurement names (e.g. 'SpineLength')
filters.source_table = []; % Index of the table that contains "var_to_filter" in your call to apply_filters
filters.comparison_method = {}; % '>', '<', '=' (e.g. '>' to keep var > threshold)
filters.filter_threshold = []; % Threshold value for the filters

% Kymograph parameters
nBins = 10; % Number of bins for nucleoid length
nTimeBins = 150; % Number of bins in time

%% Import data
load(dataset)

nucl = import_measurements(files.bacmman_folder, 2);

% Add metadata
nucl = add_metadata_info(files.bacmman_folder, varToAdd, channels, nucl);

% Apply user-defined filters
nucl = apply_filters(filters, nucl);

% Convert time values
nucl.DateTime_c0 = datetime(nucl.DateTime_c0, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS');
nucl.Time = nucl.DateTime_c0 - nucl.DateTime_c0(1);
timepoints = minutes(unique(nucl.Time));
discrete_time = discretize(minutes(nucl.Time), nTimeBins);

%% Compute kymograph

edges = linspace(0, 2, nBins/2);
kymo = zeros(length(unique(discrete_time)), length(edges)-1);

for i = 1:length(unique(discrete_time))
    nuclLengths = histcounts(nucl.SpineLength(discrete_time == i-1), edges, 'Normalization', 'probability');
    kymo(i,:) = cumsum(nuclLengths);
end

kymo = [kymo flip(kymo,2)];

%% Make figure

f1 = figure('Color', 'white', 'Position', [560 558 560 420]);
% hist3([nucl.SpineLength, minutes(nucl.Time)], 'nbins', [nBins, 249], 'CdataMode','auto');
heatmap(kymo);
colormap(jet)
% view(2)
% title(['Nucleoid length'])
xlabel('Nucleoid (um)')
ylabel('Time')
% xlim([-1 1])
% ylim([-1*2 1*2])


























