% Script to plot the fraction of cells containing 1 (or more) spots per FOV
%
% Required measurements:
% *ObjectInclusionCount* -> Containing: Bacteria; To count: Spot_detection -> Name: SpotCount
% *SpineCoordinates* -> Set SpineLength to parent: false
% *SpineFeatures* -> Bacteria; Scaled: um
%
%

cd(fileparts(matlab.desktop.editor.getActiveFilename)) % cd to script's current folder
addpath('../../Bacmman_scripts/Functions/')
addpath('../Util/')

close all
clc

%% Input

datasets = {'220202_cipro_0', '220302_cipro_30ngmL', '220302_cipro_300ngmL'};

%% Options

% Metadata (will be added to measurements table and can be used for filtering)
varToAdd = {'DateTime'}; % Metadata variables to add (e.g. 'DateTime')
channels = [0]; % Index of table containing varToAdd

% Filters (look only at a subset of the data)
% Multiple filters can be defined using arrays; leave arrays empty to apply no filter
filters.var_to_filter = {'PositionIdx'}; % Measurement names (e.g. 'SpineLength')
filters.source_table = [0]; % Index of the table that contains "var_to_filter" in your call to apply_filters
filters.comparison_method = {'<'}; % '>', '<', '=' (e.g. '>' to keep var > threshold)
filters.filter_threshold = [11]; % Threshold value for the filters

% Moving mean span
movmean_span = 5;

% Define a colormap for the figures (must have at least as many colors as datasets)
cmap = [[255 193 7];...
        [30 136 229];...
        [216 27 96];...
        [0 77 64];...
        [rand(1, 3)*255]]./255;
    
%% Processing

% Initialise figures
[f1, f2, f3, f4] = Init_figures;
p1 = [];
p2 = [];
p3 = [];

boxplot_data = [];
boxplot_groups = [];

for a = 1:length(datasets)
    
    disp(['Loading dataset ' datasets{a} '...'])
    
    % Load input for current dataset
    load(['../../../Matlab/BB_inputs/' datasets{a}])
        
    
    %% Import data
    bact = import_measurements(files.bacmman_folder, 0);
    
    % Add metadata
    bact = add_metadata_info(files.bacmman_folder, varToAdd, channels, bact);
    
    % Apply user-defined filters
    bact = apply_filters(filters, bact);
    
    % Convert time values
    bact.DateTime_c0 = datetime(bact.DateTime_c0, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS');
    bact.Time = bact.DateTime_c0 - bact.DateTime_c0(1);
    timepoints = unique(bact.Time);
    
    %% Compute metrics
    nCells = grpstats(bact.Idx, bact.PositionIdx, 'max') +1; % Number of cells per FOV
    
    spotFrac = grpstats(bact.SpotCount > 0, bact.PositionIdx, 'sum')./nCells; % Proportion of cells with spots per FOV
    spotFrac_mean = Weighted_movmean(spotFrac, movmean_span, nCells);
    
    %% General statistics
    disp(['Total number of cells: ' num2str(sum(nCells)) ' (' num2str(round(mean(nCells))) ' per FOV on average)'])
    disp(['Average proportion of cells with a spot per FOV: ' num2str(mean(spotFrac)*100,2) '%'])
    
    %% Make figures
    
    % Number of spots per cell histogram
    binRange = 0:5;
    p1(a,:) = histcounts(bact.SpotCount,[binRange Inf], 'Normalization', 'probability');
    
    % Histogram of fraction of cells with spots per FOV
    figure(f2)
    h2 = histogram(spotFrac, 'Normalization', 'probability', 'BinWidth', 0.02, 'FaceColor', cmap(a,:), 'FaceAlpha', 0.5);
    hold on
    p2(a) = plot([mean(spotFrac) mean(spotFrac)], [0 max(h2.Values)*1.25], 'LineWidth', 1.5, 'Color', cmap(a,:));
    xlabel('Fraction of cells with spots per FOV')
    ylabel('PDF')
    xlim([0 1])
    drawnow
    
    % Time evolution of the fraction of cells with spots per FOV
    figure(f3)
    hold on
    scatter(minutes(timepoints), spotFrac, 15, 'filled', 'MarkerFaceAlpha', 0.9, 'MarkerFaceColor', cmap(a,:));
    p3(a) = plot(minutes(timepoints), spotFrac_mean, 'LineWidth', 1.5, 'Color', cmap(a,:));
    hold off
    box on
    xlabel('Time (min)')
    ylabel('Proportion of cells containing an immobile spot')
    ylim([0 1])
    drawnow
    
    % Boxplot data
    boxplot_data = [boxplot_data; spotFrac];
    boxplot_groups = [boxplot_groups, repmat(a, 1, length(spotFrac))];
    
    fprintf('\n')
    
end

% Update figures
figure(f1)
b = bar(binRange, p1');
for i = 1:length(datasets)
    set(b(i),'FaceColor', cmap(i,:));
end
xlabel('Number of spots per cell')
ylabel('PDF')
legend(datasets, 'Interpreter', 'none')
legend('boxoff')

figure(f2)
hold off
legend(p2, datasets, 'Interpreter', 'none')
legend('boxoff')

figure(f3)
hold off
legend(p3, datasets, 'Interpreter', 'none')
legend('boxoff')

figure(f4)
b = boxplot(boxplot_data, boxplot_groups, 'Colors', cmap, 'Symbol', 'o', 'OutlierSize', 4, 'Labels', datasets);
set(b, 'LineWidth',2)
xtickangle(45)










