% Script to make a heatmap of spot positions in the cell (normalised width/length)
% Possibility to record a video of spot positions over time
%
% Required measurements:
% *ObjectInclusionCount* -> Containing: Bacteria; To count: Spot_detection -> Name: SpotCount
% *SpineCoordinates* -> Set SpineLength to parent: false
% *SpineFeatures* -> Bacteria; Scaled: um
%
%

cd(fileparts(matlab.desktop.editor.getActiveFilename)) % cd to script's current folder
addpath('../../Bacmman_scripts/Functions/')

close all
clc

%% Input

datasets = {'220202_cipro_0','220302_cipro_30ngmL','220302_cipro_300ngmL','220202_cipro_10ugmL','220322_cipro_0'};


%% Options

% Metadata (will be added to measurements table and can be used for filtering)
varToAdd = {'DateTime'}; % Metadata variables to add (e.g. 'DateTime')
channels = [0]; % Index of table containing varToAdd

% Filters (look only at a subset of the data)
% Multiple filters can be defined using arrays; leave arrays empty to apply no filter
filters.var_to_filter = {}; % Measurement names (e.g. 'SpineLength')
filters.source_table = []; % Index of the table that contains "var_to_filter" in your call to apply_filters
filters.comparison_method = {}; % '>', '<', '=' (e.g. '>' to keep var > threshold)
filters.filter_threshold = []; % Threshold value for the filters

% Define a colormap for the figures (must have at least as many colors as datasets)
cmap = [[230 159 0];...
        [86 180 233];...
        [0 158 115];...
        [240 228 66];...
        [0 114 178];...
        [213 94 0];...
        [204 121 167];...
        [0 0 0]]./255;
    

%% Processing

% Initialise figures
[f1, f2] = Init_figures;
p1 = [];
p2 = [];

for a = 1:length(datasets)
    
    disp(['Loading dataset ' datasets{a} '...'])
    
    % Load input for current dataset
    load(['../../../Matlab/BB_inputs/' datasets{a}])
    
    [bact, spots] = import_measurements(files.bacmman_folder, [0, 1]);
    
    % Add metadata
    [bact, spots] = add_metadata_info(files.bacmman_folder, varToAdd, channels, bact, spots);
    
    % Apply user-defined filters
    [bact, spots] = apply_filters(filters, bact, spots);
    
    % Convert time values
    bact.DateTime_c0 = datetime(bact.DateTime_c0, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS');
    bact.Time = bact.DateTime_c0 - bact.DateTime_c0(1);
    spots.DateTime_c0 = datetime(spots.DateTime_c0, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS');
    spots.Time = spots.DateTime_c0 - spots.DateTime_c0(1);
    timepoints = unique(bact.Time);
    
    
    %% Normalise spot positions
    
    % SpineCurvilinearCoord: [0:cellLength]
    % SpineRadialCoord: distance from spine (pixels)
    spots.normXpos = spots.SpineCurvilinearCoord./spots.SpineLength -0.5; % Normalise to cell length & centre
    spots.normYpos = spots.SpineRadialCoord./spots.SpineRadius; % Normalise to cell radius
    
    %% Make figures
    
    % Spot position along long axis
    binRange = -1:0.1:1;
    p1(a,:) = histcounts(spots.normXpos,[binRange Inf]);
    
    % Spot position along short axis
    binRange = -1:0.1:1;
    p2(a,:) = histcounts(spots.normYpos,[binRange Inf]);
    
end

% Update figures
figure(f1)
b = plot(binRange, p1', 'LineWidth', 1.2);
for i = 1:length(datasets)
    set(b(i),'Color', cmap(i,:));
end
line([-0.5 -0.5], [0 max(p1(:))], 'Color', 'k')
line([0.5 0.5], [0 max(p1(:))], 'Color', 'k')
xlabel('Spot position along major axis')
ylabel('PDF')
legend(datasets, 'Interpreter', 'none')
% legend('boxoff')

figure(f2)
b = plot(binRange, p2', 'LineWidth', 1.2);
for i = 1:length(datasets)
    set(b(i),'Color', cmap(i,:));
end
line([-0.5 -0.5], [0 max(p2(:))], 'Color', 'k')
line([0.5 0.5], [0 max(p2(:))], 'Color', 'k')
xlabel('Spot position along minor axis')
ylabel('PDF')
legend(datasets, 'Interpreter', 'none')
% legend('boxoff')















