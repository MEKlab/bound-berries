% Script to make a heatmap of spot positions in the cell (normalised width/length)
% Possibility to record a video of spot positions over time
% 
% Required measurements:
% *ObjectInclusionCount* -> Containing: Bacteria; To count: Spot_detection -> Name: SpotCount
% *SpineCoordinates* -> Set SpineLength to parent: false
% *SpineFeatures* -> Bacteria; Scaled: um
% 
% 

cd(fileparts(matlab.desktop.editor.getActiveFilename)) % cd to script's current folder
addpath('../../Bacmman_scripts/Functions/')

close all
clc

%% Input

dataset = '220302_cipro_30ngmL';


%% Options

% Metadata (will be added to measurements table and can be used for filtering)
varToAdd = {'DateTime'}; % Metadata variables to add (e.g. 'DateTime')
channels = [0]; % Index of table containing varToAdd

% Filters (look only at a subset of the data)
% Multiple filters can be defined using arrays; leave arrays empty to apply no filter
filters.var_to_filter = {}; % Measurement names (e.g. 'SpineLength')
filters.source_table = []; % Index of the table that contains "var_to_filter" in your call to apply_filters
filters.comparison_method = {}; % '>', '<', '=' (e.g. '>' to keep var > threshold)
filters.filter_threshold = []; % Threshold value for the filters

use_symmetry = 0; % [0/1] use this option to make a symmetrical kymograph/fork plot (centred on 0 in the major axis)

% Heatmap options
norm_heatmap = 'auto'; % 'auto' for automatic adjustment, or range (e.g. [0 100])
heatmap_title = '';

% Kymograph option
norm_kymo = 'auto'; % 'auto' for automatic adjustment, or range (e.g. [0 100])
kymo_title = heatmap_title;

% Fork plot options
norm_fork = 'auto';
fork_title = heatmap_title;

% Average spot position plot options
movmean_window = 5;
plot_color = 'b';
plot_title = heatmap_title;

% Video options
show_video = 0;
export_video = 0;
fps = 20; % Frames per second for the video export
color_range = 40;


%% Import data
load(['../../../Matlab/BB_inputs/' dataset])

[bact, spots] = import_measurements(files.bacmman_folder, [0, 1]);

% Add metadata
[bact, spots] = add_metadata_info(files.bacmman_folder, varToAdd, channels, bact, spots);

% Apply user-defined filters
[bact, spots] = apply_filters(filters, bact, spots);

% Convert time values
bact.DateTime_c0 = datetime(bact.DateTime_c0, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS');
bact.Time = bact.DateTime_c0 - bact.DateTime_c0(1);
spots.DateTime_c0 = datetime(spots.DateTime_c0, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS');
spots.Time = spots.DateTime_c0 - spots.DateTime_c0(1);
timepoints = unique(bact.Time);

% Add number of cells in FOV to spots table
for i = 1:length(unique(bact.PositionIdx))
    nCells = sum(bact.PositionIdx == i-1);
    spots.nCells(spots.PositionIdx == i-1) = nCells;
end


%% Create heatmap data

% Normalise spot positions in the cell
% SpineCurvilinearCoord: [0:cellLength]
% SpineRadialCoord: distance from spine (pixels)
spots.normXpos = spots.SpineCurvilinearCoord./spots.SpineLength -0.5; % Normalise to cell length & centre
spots.normYpos = spots.SpineRadialCoord./spots.SpineRadius; % Normalise to cell radius

mapData = [spots.normXpos spots.normYpos];

xPlot = unique(minutes(spots.Time));
yPlot = grpstats(spots.SpineCurvilinearCoord./spots.SpineLength, spots.PositionIdx, 'mean');

if use_symmetry
    kymoData = [minutes(spots.Time) abs(spots.normXpos)];
    kymoData = [kymoData; minutes(spots.Time) -abs(spots.normXpos)];
    forkData = [abs(spots.SpineCurvilinearCoord - spots.SpineLength/2), spots.SpineLength];
    forkData = [forkData; -abs(spots.SpineCurvilinearCoord - spots.SpineLength/2), spots.SpineLength];
else
    kymoData = [minutes(spots.Time) spots.normXpos];
    forkData = [spots.SpineCurvilinearCoord - spots.SpineLength/2, spots.SpineLength];
end

[f1, f3, f4, f5] = Init_figures;

%% Make heatmap of spot positions in the cell (normalised)

figure(f1);
hist3(mapData,'CdataMode','auto', 'edges', {-1:0.075:1 -2:0.15:2})
colormap(jet)
title(heatmap_title, 'Interpreter', 'tex')
xlabel('% of cell length')
ylabel('% of cell width')
colorbar
view(2)
xlim([-1 1])
ylim([-1*2 1*2])
caxis(norm_heatmap)


%% Make kymograph of spot position along long axis vs. time
figure(f3);
hist3(kymoData, 'CDataMode', 'auto', 'edges', {0:max(kymoData(:,1))/25:max(kymoData(:,1)) -1:0.08:1})
colormap(jet)
title(kymo_title, 'Interpreter', 'tex')
xlabel('Time (min)')
ylabel('% of cell length')
colorbar
view(2)
ylim([-1 1])
caxis(norm_kymo)

%% Fork plot
figure(f4)
hist3(forkData, 'CDataMode', 'auto','edges', {-2.5:0.2:2.5 1:0.16:5})
set(gca, 'YDir', 'reverse')
colormap(jet)
title(fork_title, 'Interpreter', 'tex')
xlabel('Spot position')
ylabel('Cell length (µm)')
colorbar
view(2)
xlim([-2.5 2.5])
ylim([1 5])
caxis(norm_fork)

%% Average spot position plot
figure(f5)
scatter(xPlot, yPlot, 15, 'filled', 'MarkerFaceColor', plot_color, 'MarkerFaceAlpha', 0.9)
title(plot_title)
ylim([0 1])
xlabel('Time (min)')
ylabel('% of cell length')




%% Make heatmap timelapse video

if show_video
    
    if exist('F', 'var')
        clear F
    end
    
    f2 = figure('Color', 'white', 'Position', [1120 558 560 420]);
    for i = 1:length(timepoints)-20
        figure(f2);
        
        mapData = [spots.normXpos(spots.PositionIdx >= i & spots.PositionIdx < i+20)...
            spots.normYpos(spots.PositionIdx >= i & spots.PositionIdx < i+20)];
        
        hist3(mapData,'CdataMode','auto', 'edges', {-1:0.08:1 -2:0.15:2})
        hold on
        text(0.15, 1.5, [num2str(round(minutes(timepoints(i+10)))) ' min'],...
            'Color', 'w', 'FontWeight', 'b', 'FontSize', 25)
        hold off
        colormap(jet)
        title('Position of bright spots (all cells)')
        xlabel('% of major axis')
        ylabel('% of minor axis')
        colorbar
        view(2)
        xlim([-1 1])
        ylim([-1*2 1*2])
        caxis([0 color_range])
        
        F(i) = getframe(gcf) ;
        drawnow
    end
    
end

if export_video
    
    % create the video writer with specified fps
    writerObj = VideoWriter([files.bacmman_folder 'Heatmap_video.avi']);
    writerObj.FrameRate = fps;
    writerObj.Quality = 100;
    % set the seconds per image
    % open the video writer
    open(writerObj);
    % write the frames to the video
    for i=2:length(F)
        % convert the image to a frame
        frame = F(i) ;
        writeVideo(writerObj, frame);
    end
    % close the writer object
    close(writerObj);
    
end




















